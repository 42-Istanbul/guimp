NAME = guimp
SRCS = main.c
OBJS = $(SRCS:.o=.c)

CC = gcc
RM = rm -rf
CFLAGS = -Wall -Wextra -Werror
LFLAGS = `sdl2-config --libs --cflags`
INCLUDES = -I./libui/include 

all: $(NAME)

debug: CFLAGS += -g
debug: $(NAME)

address: CFLAGS += -fsanitize=address -g
address: re

$(NAME): $(OBJS)
	$(CC) $(CFLAGS) $(OBJS) -o $(NAME) $(LFLAGS)  $(INCLUDES)

%.o: %.c
	$(CC) $(CFLAGS) -c $< -o $@ $(INCLUDES)

clean:
	$(RM) *.o

fclean: clean
	$(RM) $(NAME)

re: fclean all

.PHONY: all clean fclean re debug address
